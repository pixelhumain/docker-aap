# docker crontab

## fichiers et repertoire

### Dockerfile

### crontab
l'ideal serait un nom de domaine en https à la place de http://front:80

```
MAILTO = ""
* * * * * wget -O /dev/null $PARAM_cronurl/co2/cron/docron?crontoken=$PARAM_crontoken
@hourly wget -O /dev/null $PARAM_cronurl/co2/cron/checkdeletepending?crontoken=$PARAM_crontoken
@daily wget -O /dev/null $PARAM_cronurl/co2/mailmanagement/updatetopending?crontoken=$PARAM_crontoken

* * * * * wget -O /dev/null $PARAM_cronurl/api/activitypub/processcron?crontoken=$PARAM_crontoken
```

pour activitypub c'est compliqué car il y a une redir du domaine racine vers www

## env
```env
PARAM_crontoken = 
PARAM_cronurl=http://front:80
```

## commande pour mettre à jour repo et build image

```sh
VERSION=1.0.2 ./build.sh
```