#!/bin/sh
set -e

if [ -z "${VERSION}" ]; then
# Définir VERSION comme un horodatage (format : YYYYMMDDHHMMSS)
VERSION=$(date +"%Y%m%d%H%M%S")
fi

TAG="latest"

docker build --pull --rm -t communecter/communecter-cron:$VERSION .
docker tag communecter/communecter-cron:$VERSION communecter/communecter-cron:$VERSION
docker push communecter/communecter-cron:$VERSION
docker tag communecter/communecter-cron:$VERSION communecter/communecter-cron:$TAG
docker push communecter/communecter-cron:$TAG



