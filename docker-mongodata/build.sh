#!/bin/sh
set -e

if [ -z "${VERSION}" ]; then
# Définir VERSION comme un horodatage (format : YYYYMMDDHHMMSS)
VERSION=$(date +"%Y%m%d%H%M%S")
fi

TAG="latest"

docker build --pull --rm -t communecter/communecter-mongodata:$VERSION .
docker tag communecter/communecter-mongodata:$VERSION communecter/communecter-mongodata:$VERSION
docker push communecter/communecter-mongodata:$VERSION
docker tag communecter/communecter-mongodata:$VERSION communecter/communecter-mongodata:$TAG
docker push communecter/communecter-mongodata:$TAG



