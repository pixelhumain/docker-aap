#!/bin/bash

cp -rf ../docker-front-apache/code/modules/co2/data/lists.json lists.json
cp -rf ../docker-front-apache/code/modules/co2/data/cities.json.zip cities.json.zip
cp -rf ../docker-front-apache/code/modules/co2/data/zones.json.zip zones.json.zip
cp -rf ../docker-front-apache/code/modules/co2/data/translate.json.zip translate.json.zip
