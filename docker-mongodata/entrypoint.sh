#!/bin/bash
set -e

# This script checks if the container is started for the first time.

CONTAINER_FIRST_STARTUP="CONTAINER_FIRST_STARTUP"
if [ ! -e /$CONTAINER_FIRST_STARTUP ]; then
    touch /$CONTAINER_FIRST_STARTUP
    # place your script that you only want to run on first startup.

    MONGO_dbname=${MONGO_dbname:-communecter}

generate_mongo_command() {
    command="mongo"
    if [ -n "$MONGO_host" ]; then
        command="$command --host $MONGO_host"
    fi
    if [ -n "$MONGO_port" ]; then
        command="$command --port $MONGO_port"
    fi
    if [ -n "$MONGO_username" ]; then
        command="$command -u $MONGO_username"
    fi
    if [ -n "$MONGO_password" ]; then
        command="$command -p$MONGO_password"
    fi
    if [ -n "$MONGO_authSource" ]; then
        command="$command --authenticationDatabase $MONGO_authSource"
    fi
    if [ -n "$MONGO_dbname" ]; then
        command="$command $MONGO_dbname"
    fi

    echo $command
}

generate_mongoimport_command() {
    command="mongoimport"
    if [ -n "$MONGO_host" ]; then
        command="$command --host=$MONGO_host"
    fi
    if [ -n "$MONGO_port" ]; then
        command="$command --port=$MONGO_port"
    fi
    if [ -n "$MONGO_username" ]; then
        command="$command --username=$MONGO_username"
    fi
    if [ -n "$MONGO_password" ]; then
        command="$command --password=$MONGO_password"
    fi
    if [ -n "$MONGO_authSource" ]; then
        command="$command --authenticationDatabase=$MONGO_authSource"
    fi
    if [ -n "$MONGO_dbname" ]; then
        command="$command --db=$MONGO_dbname"
    fi
    echo $command
}

check_collection_exists() {
    collection_name=$1
    commandtest=$(generate_mongo_command)
    commandtest="$commandtest --quiet --eval \"db.getCollectionNames().indexOf('$collection_name')\""
    index=$(eval $commandtest)
    if [ $index -ne -1 ]; then
        return 0
    else
        return 1
    fi
}


register_user() {
    if [ -n "$PARAM_co_username" ] && [ -n "$PARAM_co_password" ] && [ -n "$PARAM_co_name" ] && [ -n "$PARAM_co_email" ]; then

        if [ ${#PARAM_co_password} -gt 8 ]; then

            co_username="$PARAM_co_username"
            co_password="$PARAM_co_password"
            co_name="$PARAM_co_name"
            co_email="$PARAM_co_email"
            co_organization_name="$PARAM_co_organization_name"

            # Création de l'utilisateur
            curl_response=$(curl -s 'http://front:80/co2/person/register' \
              -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,zh;q=0.6,zh-CN;q=0.5' \
              -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
              -H 'X-Requested-With: XMLHttpRequest' \
              --data-urlencode "name=$co_name" \
              --data-urlencode "username=$co_username" \
              --data-urlencode "email=$co_email" \
              --data-urlencode "pwd=$co_password" \
              --data-urlencode "app=co2" \
              --data-urlencode "pendingUserId=" \
              --data-urlencode "mode=two_steps_register" \
              --data-urlencode "isInvitation=true")

            # Recherche de l'utilisateur dans la base MongoDB par email
            citoyen_id=$(echo "
                var result = db.citoyens.findOne({\"email\": \"$co_email\"}, {\"_id\": 1});
                print(result._id);
            " | eval $command | grep -oE '[a-f0-9]{24}')

            if [ -n "$citoyen_id" ]; then

                echo "Utilisateur créé avec succès."
                # Mise à jour de l'utilisateur avec le rôle de superAdmin
                echo "
                    db.citoyens.update(
                        {\"email\": \"$co_email\"},
                        {\"\$set\": {\"roles.superAdmin\": true}, \"\$unset\": {\"roles.tobeactivated\": \"\"}}
                    );
                " | eval $command

                # Vérification si PARAM_co_organization_name est présent
                if [ -n "$co_organization_name" ]; then
                    echo "Création de l'organisation..."
                    # Création de l'organisation
                    created=$(date +%s)
                    modified=$(date --utc +"%Y-%m-%dT%H:%M:%S.000Z")
                    slug=$(echo "$co_organization_name" | tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9]/-/g')

                    echo "
                        var created = NumberLong(Math.floor(Date.now() / 1000));
                        var updated = created;
                        var result = db.organizations.insert({
                            collection: 'organizations',
                            created: created,
                            creator: '$citoyen_id',
                            modified: ISODate('$modified'),
                            name: '$co_organization_name',
                            preferences: {
                                isOpenData: 'true',
                                isOpenEdition: 'true'
                            },
                            role: 'admin',
                            slug: '$slug',
                            type: 'Group',
                            updated: updated,
                            links: {
                                members: {
                                    '$citoyen_id': {
                                        date: ISODate('$modified'),
                                        isAdmin: true,
                                        type: 'citoyens'
                                    }
                                }
                            },
                            oceco: {
                                pole: false,
                                organizationAction: true,
                                projectAction: true,
                                eventAction: true,
                                commentsAction: true,
                                memberAuto: false,
                                memberValidationInviteAuto: false,
                                contributorValidationInviteAuto: false,
                                agenda: false,
                                costum: {
                                    projects: { form: { geo: false } },
                                    events: { form: { geo: false } },
                                    actions: { form: { min: false, max: false, optionPodomoro: false, optionTask: false } }
                                },
                                spendView: false,
                                notificationChat: false,
                                membersAdminProjectAdmin: false,
                                contributorAuto: true,
                                attendeAuto: true,
                                memberAddAction: true,
                                spendNegative: false,
                                subOrganization: false,
                                milestonesProject: false,
                                gitProject: false,
                                organizationForms: false,
                                projectForms: false
                            }
                        });
                    " | eval $command

                    org_id=$(echo "
                        var result = db.organizations.findOne({\"slug\": \"$slug\"}, {\"_id\": 1});
                        print(result._id);
                    " | eval $command | grep -oE '[a-f0-9]{24}')

                    # Mise à jour de l'utilisateur pour inclure l'organisation
                    if [ -n "$org_id" ]; then
                    echo "Création de l'organisation réussie."
                        echo "
                            db.citoyens.update(
                                {\"_id\": ObjectId(\"$citoyen_id\")},
                                {\"\$set\": {\"links.memberOf.$org_id\": {\"date\": ISODate('$modified'), \"isAdmin\": true, \"type\": \"organizations\"}}}
                            );
                        " | eval $command
                    else
                        echo "Erreur lors de la création de l'organisation"
                    fi
                fi
            else
                echo "Erreur : impossible de récupérer l'ID du citoyen."
            fi
        fi
    else
        echo "Paramètres manquants ou mot de passe trop court."
    fi
}


command=$(generate_mongo_command)

commandimport=$(generate_mongoimport_command)

# Attendre que MongoDB soit prêt à accepter les connexions
echo "Attente que MongoDB soit prêt à accepter les connexions..."
while ! nc -z "$MONGO_host" "$MONGO_port"; do
    echo "MongoDB n'est pas prêt, en attente..."
    sleep 5
done
echo "MongoDB est maintenant prêt."

if check_collection_exists "application"; then
    echo "Collection 'application' exists in database '$MONGO_dbname'."
else
    echo "Collection 'application' does not exist in database '$MONGO_dbname'."
    
if [ -n "$MONGO_createuser" ] && [ -n "$MONGO_username" ] && [ -n "$MONGO_password" ] && [ -n "$MONGO_dbname" ]; then
echo "create user ... "
eval $command <<EOF
db.createUser({ user: "$MONGO_username", pwd: "$MONGO_password", roles: [ { role: "readWrite", db: "$MONGO_dbname" } ] });
EOF
fi

eval $command <<EOF
db.lists.dropIndexes();
db.lists.remove({});
EOF

eval $commandimport  --collection=lists "/opt/lists.json" --jsonArray

#Test cities.json
if [ -f "/opt/cities.json" ];then
 rm "/opt/cities.json"
fi

if [ -f "/opt/cities.json.zip" ];then

unzip "/opt/cities.json.zip" -d "/opt/"

#delete cities and delete all index cities
eval $command  <<EOF
db.cities.dropIndexes();
db.cities.remove({});
EOF

commandimportcities=$(generate_mongoimport_command)

echo "Import cities data..."
#import cities
eval $commandimport  --collection=cities "/opt/cities.json"
fi

#Test zones.json
if [ -f "/opt/zones.json" ];then
 rm "/opt/zones.json"
fi

if [ -f "/opt/zones.json.zip" ];then

unzip "/opt/zones.json.zip" -d "/opt/"

#delete cities and delete all index cities
eval $command  <<EOF
db.zones.dropIndexes();
db.zones.remove({});
EOF

echo "Import zones data..."
#import zones
commandimportzones=$(generate_mongoimport_command)

eval $commandimport --collection=zones "/opt/zones.json"
fi

unzip /opt/translate.json.zip -d /opt/

eval $command <<EOF
db.translate.dropIndexes();
db.translate.remove({});
EOF

eval $commandimport --collection=translate "/opt/translate.json"

eval $command <<EOF
db.createCollection("applications")
db.applications.insert({     "_id" : ObjectId("59f1920bc30f30536124355d"),     "name" : "DEV Config",     "key" : "devParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
db.applications.insert({     "_id" : ObjectId("59f1920bc30f30536124355e"),     "name" : "PROD Config",     "key" : "prodParams",     "mangoPay" : {         "ClientId" : "communecter",         "ClientPassword" : "xxxx",         "TemporaryFolder" : "../../tmp"     } } )
EOF


eval $command <<EOF
db.citoyens.dropIndexes();
db.cities.dropIndexes();
db.events.dropIndexes();
db.organizations.dropIndexes();
db.projects.dropIndexes();
EOF


eval $command <<EOF
db.citoyens.createIndex({"email": 1} , { unique: true });
db.cities.createIndex({"geoPosition": "2dsphere"});
db.cities.createIndex({"postalCodes.geoPosition": "2dsphere"});
db.cities.createIndex({"geoShape" : "2dsphere" });
db.cities.createIndex({"insee" : 1});
db.cities.createIndex({"region" : 1});
db.cities.createIndex({"dep" : 1});
db.cities.createIndex({"cp" : 1});
db.cities.createIndex({"country" : 1});
db.cities.createIndex({"postalCodes.name" : 1});
db.cities.createIndex({"postalCodes.postalCode" : 1});
db.events.createIndex({"geoPosition" : "2dsphere" });
db.events.createIndex({"parentId" : 1});
db.events.createIndex({"name" : 1});
db.organizations.createIndex({"geoPosition" : "2dsphere" });
db.projects.createIndex({"geoPosition" : "2dsphere" });
db.citoyens.createIndex({"geoPosition" : "2dsphere" });
EOF

eval $command <<EOF
db.createCollection("costum")
db.costum.insert({
    "_id" : ObjectId("606469e3feca965bce1025bf"),
    "slug" : "costumize",
    "welcomeTpl" : "costum.views.custom.costumize.home",
    "sourceKey" : true,
    "favicon" : "",
    "metaImg" : "",
    "metaTitle" : "",
    "logoMin" : "",
    "formId" : "projetId",
    "docId" : "id",
    "admin" : {
        "email" : ""
    },
    "js" : {
        "urls" : [ 
            "costumize.js"
        ]
    },
    "dynForm" : {
        "jsonSchema" : {
            "title" : "Template config",
            "type" : "object"
        }
    },
    "css" : {
        "urls" : [ 
            "costumize.css"
        ],
        "loader" : {
            "background" : "white",
            "ring1" : {
                "color" : "#3a8037",
                "height" : 360,
                "width" : 360,
                "left" : -15,
                "borderWidth" : 4,
                "top" : -35
            },
            "ring2" : {
                "color" : "#f8904a",
                "height" : 350,
                "width" : 350,
                "left" : -10,
                "borderWidth" : 5,
                "top" : -30
            }
        },
        "progress" : {
            "value" : {
                "background" : "#3a8037"
            },
            "bar" : {
                "background" : "#fff"
            }
        },
        "menuTop" : {
            "background" : "white",
            "boxShadow" : "1px 1px 3px #ddd",
            "logo" : {
                "height" : 70
            },
            "app" : {
                "paddingLeft" : "30px",
                "button" : {
                    "fontSize" : 20,
                    "fontWeight" : 800,
                    "paddingBottom" : "0px",
                    "marginLeft" : "10px",
                    "textTransform" : "capitalize",
                    "color" : "#000000"
                }
            },
            "button" : {
                "paddingTop" : 0,
                "fontSize" : 30,
                "color" : "#5b2649"
            },
            "scopeBtn" : {
                "color" : "#5b2649"
            },
            "filterBtn" : {
                "color" : "#5b2649"
            },
            "badge" : {
                "background" : "#5b2649",
                "border" : "1px solid white"
            },
            "connectBtn" : {
                "background" : "#5b2649",
                "color" : "white",
                "fontSize" : 18,
                "borderRadius" : 10,
                "padding" : "8px 15px"
            }
        },
        "menuApp" : {
            "background" : "#3a8037",
            "button" : {
                "fontSize" : 80,
                "color" : "#008037",
                "hover" : {
                    "borderBottom" : "2px solid #fff"
                }
            }
        },
        "button" : {
            "footer" : {
                "add" : {
                    "paddingLeft" : "10px",
                    "fontSize" : 30,
                    "bottom" : 1000,
                    "background" : "transparent",
                    "color" : "#098048"
                },
                "toolbarAdds" : {
                    "bottom" : 10,
                    "paddingLeft" : "40px",
                    "fontSize" : 50,
                    "left" : "0"
                }
            }
        },
        "color" : {
            "purple" : "#5b2649",
            "blue" : "#0091c6",
            "orange" : "#fea621"
        }
    },
    "app" : [],
    "htmlConstruct" : {
        "element" : {
            "tplCss" : [ 
                "v1.0_pageProfil.css"
            ],
            "banner" : "co2.views.element.bannerHorizontal",
            "containerClass" : {
                "centralSection" : "col-xs-12 col-lg-10 col-lg-offset-1"
            },
            "menuTop" : {
                "class" : "col-xs-12",
                "left" : {
                    "class" : "col-lg-3 col-md-3 col-sm-4 hidden-xs no-padding",
                    "buttonList" : []
                },
                "right" : {
                    "class" : "col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center",
                    "buttonList" : {
                        "xsMenu" : {
                            "class" : "visible-xs",
                            "buttonList" : []
                        },
                        "app" : {
                            "class" : "hidden-xs",
                            "label" : true,
                            "icon" : false,
                            "spanTooltip" : false,
                            "buttonList" : []
                        },
                        "params" : {
                            "buttonList" : {
                                "settings" : true
                            }
                        }
                    }
                }
            }
        },
        "adminPanel" : {
            "add" : true,
            "menu" : {
                "statistic" : true,
                "directory" : true,
                "reference" : {
                    "label" : "Référencement",
                    "show" : true,
                    "initType" : [ 
                        "projects"
                    ],
                    "icone" : "list"
                },
                "community" : {
                    "show" : true,
                    "label" : "Membres"
                },
                "mails" : true
            }
        },
        "directoryViewMode" : "block",
        "footer" : {
            "url" : "costum.views.custom.costumize.footer"
        },
        "redirect" : {
            "logged" : "welcome",
            "unlogged" : "welcome"
        }
    },
    "typeObj" : [],
    "filters" : {
        "sourceKey" : true
    }
} )
EOF

register_user

fi

fi