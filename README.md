# Utilisation de Docker pour créer une image et la publier régulièrement

Cette documentation décrit les étapes nécessaires pour créer une image Docker à partir de votre code, la mettre à jour et la publier régulièrement, ainsi que pour utiliser Docker-Compose pour créer des versions locales et de déploiement de votre application.

## Fichiers et répertoires

Voici la liste des fichiers et répertoires nécessaires pour cette configuration :

- `prepare.sh` : script shell pour créer les répertoires et fichiers nécessaires pour Docker-Compose.
- `docker-compose.yml` (version locale) : utilisé pour lancer l'application en local. Pour lancer l'application, utilisez la commande `docker-compose -f "docker-compose.yml" up -d --build`.
- `docker-compose-traefik.yml` (version de déploiement) : utilisé pour déployer l'application sur un serveur. Ce fichier doit être modifié pour inclure le nom de domaine de votre application. Pour déployer l'application, utilisez la commande `docker-compose -f "docker-compose-traefik.yml" --env-file ./.env.prod up -d --build`.
- `.env` : fichier contenant les variables d'environnement utilisées par Docker-Compose et les conteneurs Docker.
- `.env.prod` : copie de `.env`, configuré avec toutes les informations sur la base de données et les différents comptes (SMTP, Mapbox, API2, OpenBadge, RocketChat, etc.).
- `docker-front-apache` : répertoire contenant les fichiers de configuration pour générer le Dockerfile de l'application.
- `docker-cron` : répertoire contenant les fichiers de configuration pour générer le Dockerfile pour les tâches cron.
- `docker-mongodata` : répertoire contenant les données de la base de données MongoDB pour générer le Dockerfile.
- `docker-api2` : répertoire contenant les fichiers de configuration pour générer le Dockerfile pour l'API2.
- `docker-oceco` : répertoire contenant les fichiers de configuration pour générer le Dockerfile pour Oceco.

## Lancer en Local avec Docker-Compose sans Reconstruction d'Image

1. Copiez `.env` en `.env.prod` et configurez ce dernier avec les informations de votre environnement (base de données, comptes SMTP, etc.).
   ```bash
   cp .env .env.prod
   ```
2. Exécutez `prepare.sh` pour créer les répertoires et fichiers requis :
   ```bash
   ./prepare.sh
   ```
3. Si vous utilisez des images Docker existantes et ne souhaitez pas les reconstruire, suivez les étapes suivantes :
   
   ```bash
   docker-compose -f "docker-compose.yml" --env-file ./.env.prod up -d
   ```
   Cette commande utilise directement les images Docker actuelles sans les reconstruire, pour un lancement rapide de l’application en local.

4. Pour arrêter l'exécution, utilisez :
   
   ```bash
   docker-compose -f "docker-compose.yml" --env-file ./.env.prod down

## Lancer en Local avec Reconstruction d'Image

Si vous souhaitez reconstruire les images pour tester des modifications locales, vous pouvez :

1. Décommenter les lignes de build dans le fichier `docker-compose.yml`.
2. Lancer la commande suivante pour reconstruire les images et exécuter l’application en local :
   
   ```bash
   docker-compose -f "docker-compose.yml" up -d --build
   ```


## Déploiement avec Traefik

Pour déployer votre application avec Traefik, vous aurez besoin d'une instance Linux, de Docker et de Docker-Compose installés, d'un nom de domaine et d'un serveur DNS avec une zone, ainsi que de l'adresse IP de l'instance. Vous devrez également configurer un enregistrement A vers l'adresse IP de l'instance, ou un CNAME si vous utilisez un autre nom de domaine.

1. Copiez `.env` en `.env.prod` et configurez ce dernier avec les informations de votre environnement (base de données, comptes SMTP, etc.).
   ```bash
   cp .env .env.prod
   ```
2. Exécutez `prepare.sh` pour créer les répertoires et fichiers requis :
   ```bash
   ./prepare.sh
   ```
3. utilisez la commande `docker-compose -f "docker-compose-traefik.yml" --env-file ./.env.prod up -d --build`

### mise à jour

```sh
sudo docker-compose -f "docker-compose-traefik.yml" --env-file ./.env.prod stop
sudo docker-compose -f "docker-compose-traefik.yml" --env-file ./.env.prod pull
sudo docker-compose -f "docker-compose-traefik.yml" --env-file ./.env.prod up -d --remove-orphans
sudo docker image prune
```

### Build des images localement et push
Pour build les images localement et les push sur un registry, vous pouvez utiliser les scripts suivants :

Exécuter `./prepare.sh` pour créer les répertoires et fichiers nécessaires pour docker-compose.
Se rendre dans le répertoire `docker-front-apache/` et exécuter `./copyrepo.sh` pour copier le code de l'application dans le répertoire approprié. Ensuite, exécuter `VERSION=1.0.0 ./build.sh` pour construire l'image.
Se rendre dans le répertoire `docker-cron/` et exécuter `VERSION=1.0.0 ./build.sh` pour construire l'image.
Se rendre dans le répertoire `docker-mongodata/` et exécuter `./copydata.sh` pour copier les données de la base de données dans le répertoire approprié. Ensuite, exécuter `VERSION=1.0.0 ./build.sh` pour construire l'image.
Se rendre dans le répertoire `docker-api2/` et exécuter `./copyrepo.sh` pour copier le code de l'API2 dans le répertoire approprié. Ensuite, exécuter `VERSION=1.0.0 ./build.sh` pour construire l'image.
Se rendre dans le répertoire `docker-oceco/` et exécuter `./copyrepo.sh` pour copier le code de Oceco dans le répertoire approprié. Ensuite, exécuter `VERSION=1.0.0 ./build.sh` pour construire l'image.
Exécuter `docker-compose -f "docker-compose.yml" up -d --build` pour lancer les conteneurs construits à partir des images.
Une fois les images construites, vous pouvez les tagger avec la version appropriée et les pousser sur un registry. Vous pouvez ensuite utiliser ces images pour déployer l'application sur un environnement de production en utilisant docker-compose-traefik.yml.

Il est important de noter que les scripts et les fichiers de configuration présentés ici sont spécifiques à une application donnée et peuvent varier en fonction des besoins de votre projet. Il est donc important de les adapter en fonction de vos besoins.


## Personalisation des fichiers
dans volumes
```yaml
    volumes:
      - ./docker-front-apache/conf-yii/.htaccess:/code/pixelhumain/ph/.htaccess
      - ./docker-front-apache/conf-yii/`paramsconfig.php`:/code/pixelhumain/ph/protected/config/paramsconfig.php
      - ./docker-front-apache/conf-yii/config_yii2.php:/code/pixelhumain/ph/protected/config/yii2/web.php
      - ./docker-front-apache/conf-yii/key.pem:/code/pixelhumain/ph/protected/config/certificate/key.pem
      - ./docker-front-apache/conf-yii/public.pem:/code/pixelhumain/ph/protected/config/certificate/public.pem
      - ./docker-front-apache/conf-yii/robots.txt:/code/pixelhumain/ph/robots.txt
      - ./docker-front-apache/conf-yii/DoCronAction.php:/code/modules/citizenToolKit/controllers/cron/DoCronAction.php
      - ./docker-front-apache/conf-yii/index.php :/code/pixelhumain/ph/index.php
```

## build des images localement et push
```bash
./prepare.sh
cd docker-front-apache/
./copyrepo.sh
VERSION=1.0.0 ./build.sh
cd ..
cd docker-cron/
VERSION=1.0.0 ./build.sh
cd ..
cd docker-mongodata/
./copydata.sh
VERSION=1.0.1 ./build.sh
cd ..
cd docker-api2/
./copyrepo.sh
VERSION=1.0.0 ./build.sh
cd ..
cd docker-oceco/
./copyrepo.sh
VERSION=1.0.0 ./build.sh
cd ..
docker-compose -f "docker-compose.yml" up -d --build
```

## ENV

La configuration de l'application peut être effectuée en utilisant des variables d'environnement. Les variables d'environnement suivantes sont utilisées pour configurer l'application :


`DOMAIN`: Domaine de l'application (exemple : communecter.org)  
`acme_email`: Adresse email utilisée pour les certificats SSL Let's Encrypt.  
`MONGO_dsn`: DSN de la base de données MongoDB.  
`MONGO_INITDB_ROOT_USERNAME`: Nom d'utilisateur pour l'utilisateur root de MongoDB.  
`MONGO_INITDB_ROOT_PASSWORD`: Mot de passe pour l'utilisateur root de MongoDB.  
`MONGO_dbname`: Nom de la base de données MongoDB.  
`MONGO_username`: Nom d'utilisateur pour la base de données MongoDB.  
`MONGO_password`: Mot de passe pour la base de données MongoDB.  
`MONGO_port`: Port utilisé pour se connecter à la base de données MongoDB.  
`MONGO_host`: Hôte de la base de données MongoDB.  
`MONGO_authSource`: Source d'authentification pour MongoDB.  
`MONGO_createuser`: Indicateur pour créer un utilisateur pour la base de données MongoDB.
`YII_prod`: Indicateur pour activer le mode production.  
`PARAM_mail_host`: Hôte SMTP pour l'envoi de courriels.  
`PARAM_mail_username`: Nom d'utilisateur pour l'authentification SMTP.  
`PARAM_mail_password`: Mot de passe pour l'authentification SMTP.  
`PARAM_mail_port`: Port pour l'authentification SMTP.  
`PARAM_mail_encryption`: Type de chiffrement pour l'authentification SMTP.    
`PARAM_adminEmail`: Adresse email de l'administrateur.  
`PARAM_contactEmail`: Adresse email de contact.   
`PARAM_name`: Nom de l'application.  
`PARAM_crontoken`: Token utilisé pour l'exécution des tâches planifiées.  
`PARAM_cronurl`: URL pour l'exécution des tâches planifiées.  
`PARAM_mapboxActive`: Indicateur pour activer Mapbox.  
`PARAM_forceMapboxActive`: Indicateur pour forcer l'activation de Mapbox.  
`PARAM_mapboxToken`: le token d'accès à l'API Mapbox.  
`PARAM_gulp`: indique si gulp est activé pour la compilation des fichiers CSS et JS.  
`PARAM_rocketchatEnabled`: indique si l'intégration avec Rocket.Chat est activée.  
`PARAM_rocketchatMultiEnabled`: indique si l'intégration avec Rocket.Chat est activée pour les utilisateurs multiples.  
`PARAM_rocketchatURL`: l'URL de l'instance Rocket.Chat.  
`PARAM_rocketAdmin`: le nom d'utilisateur de l'administrateur Rocket.Chat.  
`PARAM_rocketAdminPwd`: le mot de passe de l'administrateur Rocket.Chat.  
`PARAM_adminLoginToken`: le jeton d'authentification pour l'administrateur Rocket.Chat.  
`PARAM_adminRocketUserId`: l'ID de l'utilisateur administrateur Rocket.Chat.  
`PARAM_openbadge_url`: l'URL de l'instance OpenBadge.  
`PARAM_co_username`: nom d'utilisateur pour la création d'un compte utilisateur.  
`PARAM_co_password`: mot de passe pour la création d'un compte utilisateur (8 caractères minimum).  
`PARAM_co_name`: nom pour la création d'un compte utilisateur.  
`PARAM_co_email`: email pour la création d'un compte utilisateur.
`PARAM_co_organization_name`: nom de l'organisation pour la création d'une organisation. (il faut créer un user par defaut pour pouvoir créer une organisation, utile pour oceco)
`PARAM_jwt_secretKey`: clé secrète pour les jetons JWT.  
`PARAM_jwt_alg`: algorithme de chiffrement pour les jetons JWT.  
`PARAM_jwt_refreshTokenKey`: clé secrète pour les jetons de rafraîchissement JWT.  
`PARAM_jwt_exchangeTokenKey`: clé secrète pour les jetons d'échange JWT.
`PARAM_jwt_schema`: schéma utilisé pour les jetons JWT.  
`PARAM_jwt_domain`: domaine utilisé pour les jetons JWT.  
`PARAM_jwt_whiteListServer`: liste blanche des serveurs autorisés pour les jetons JWT. (séparés par des virgules)
`PARAM_activitypub_schema`: schéma utilisé pour les activités ActivityPub.  
`PARAM_activitypub_host`: hôte utilisé pour les activités ActivityPub.  
`PARAM_cows_enable`: Indicateur pour activer les notifications via WebSocket.  
`PARAM_cows_serverUrl`: URL du serveur WebSocket.  
`PARAM_cows_pingNorificationUrl`: URL pour les notifications via WebSocket.  
`PARAM_cows_pingNewPostUrl`: URL pour les nouveaux messages via WebSocket.  
`PARAM_cows_pingRefreshCoformAnswerUrl`: URL pour rafraîchir les réponses aux formulaires via WebSocket.
`PARAM_cows_pingNewEventUrl`: URL pour les nouveaux événements via WebSocket.
`PARAM_cows_pingRefreshViewUrl`: URL pour rafraîchir la vue via WebSocket.
`PARAM_cows_pingActionManagement`: URL pour la gestion des actions via WebSocket.
`PARAM_cows_pingUpdateCostum`: URL pour mettre à jour les costum via WebSocket.
`PARAM_cows_pingStripeCheckout`: URL pour les paiements Stripe via WebSocket.  
`PARAM_cows_pingCommentUrl`: URL pour les commentaires via WebSocket.

Si client `PARAM_cows_serverUrl_client` n'est pas definis alors ce sont les valeurs `PARAM_cows_*` qui sont utilisées pour la partie client.
autrement les valeurs suivantes sont utilisées pour la partie client. (pour le service api2 dans docker-compose.yml c'est necessaire)  

`PARAM_cows_serverUrl_client`: `ws://localhost:5083` - Utilisée pour spécifier l'URL du serveur WebSocket local.  
`PARAM_cows_pingNorificationUrl_client`: `http://localhost:5083/wsco/ping-new-notification` - Pour envoyer des notifications.  
`PARAM_cows_pingNewPostUrl_client`: `http://localhost:5083/wsco/ping-new-post` - Pour signaler un nouveau message.  
`PARAM_cows_pingRefreshCoformAnswerUrl_client`: `http://localhost:5083/wsco/ping-refresh-coform-answer` - Pour rafraîchir les réponses aux formulaires.  
`PARAM_cows_pingNewEventUrl_client`: `http://localhost:5083/wsco/ping-new-event` - Pour signaler un nouvel événement.  
`PARAM_cows_pingRefreshViewUrl_client`: `http://localhost:5083/wsco/ping-refresh-view` - Pour rafraîchir la vue.  
`PARAM_cows_pingActionManagement_client`: `http://localhost:5083/wsco/ping-action-management` - Pour la gestion des actions.  
`PARAM_cows_pingUpdateCostum_client`: `http://localhost:5083/wsco/ping-update-costum` - Pour mettre à jour un costum.  
`PARAM_cows_pingStripeCheckout_client`: `http://localhost:5083/wsco/ping-stripe-checkout` - Pour gérer les paiements Stripe.
L'image Docker pour l'API2 nécessite également certaines variables d'environnement spécifiques pour être configurée.
`PARAM_cows_pingCommentUrl_client`: `http://localhost:5083/wsco/ping-comment` - Pour les commentaires.

`NODE_ENV`: `production` - Spécifie l'environnement d'exécution, ici en mode production.  
`JWT_SECRET`: `secret` - Clé secrète utilisée pour signer les JWT (vous devriez changer cette valeur en production).  
`PORT_SERVER`: `3000` - Spécifie le port sur lequel le serveur doit écouter.  
`HOST_TO_FETCH`: `http://front:80` - Spécifie l'URL du front-end, ici il s'agit de l'adresse du conteneur `front` sur le port 80.  
`MONGO_URL`: `mongodb://admin:password@mongo42:27017/api2?authSource=admin` - Connexion à la base de données MongoDB, incluant le nom d’utilisateur, mot de passe, l’hôte, et la base de données.  

L'image Docker pour Oceco nécessite également certaines variables d'environnement spécifiques pour être configurée.
`ROOT_URL`: `http://localhost` - URL racine de l'application.
`MONGO_URL`: `mongodb://admin:password@mongo42:27017/communecter?authSource=admin` - Connexion à la base de données MongoDB de communecter, incluant le nom d’utilisateur, mot de passe, l’hôte, et la base de données.
`MONGO_OPLOG_URL`: `mongodb://admin:password@mongo42:27017/local?authSource=admin` - Connexion à l'oplog de la base de données MongoDB, incluant le nom d’utilisateur, mot de passe, l’hôte, et la base de données. (optionnel)
`PORT` : `3000` - Port sur lequel le serveur doit écouter.
`METEOR_SETTINGS`: `'{"module":"co2","endpoint":"http://front:80","public":{"urlimage":"http://localhost:5080"}}'` - Paramètres de configuration pour Meteor, incluant le module, l'endpoint, et l'URL de l'image, et d'autres paramètres possible spécifiques à l'application.

Ceci est un exemple de documentation des variables d'environnement pour l'application Communecter. Il est important de noter que certaines variables peuvent ne pas être nécessaires dans tous les cas d'utilisation et peuvent varier en fonction de la configuration spécifique de l'application.
