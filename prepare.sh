#!/bin/bash

# Créez les dossiers uniquement s'ils n'existent pas déjà
# for dir in upload runtime log assets; do
#     if [ ! -d "$dir" ]; then
#         echo "Création du dossier $dir..."
#         mkdir "$dir"
#         chown -R $USER:$USER "$dir"
#         chmod -R 777 "$dir"
#     else
#         echo "Le dossier $dir existe déjà, pas de modification."
#     fi
# done

# Créez le fichier acme.json uniquement s'il n'existe pas déjà
if [ ! -f "acme.json" ]; then
    echo "Création de acme.json..."
    touch acme.json
    chmod 600 acme.json
else
    echo "Le fichier acme.json existe déjà, pas de modification."
fi

echo "Configuration de préparation terminée."
