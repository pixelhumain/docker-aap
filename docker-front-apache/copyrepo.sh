#!/bin/bash

#cp -rf ~/pixelhumain-docker-php7/code/modules/ ~/meteor/communecter/docker-aap/docker-front-apache/code
#cp -rf ~/pixelhumain-docker-php7/code/pixelhumain/ ~/meteor/communecter/docker-aap/docker-front-apache/code

if [ -d "${PWD}/code/" ]
then
echo "code déja présent"
else
  mkdir code
fi

cd code/

if [ -d "${PWD}/pixelhumain/" ]
then
echo "pixelhumain déja présent"
else
  git clone https://gitlab.adullact.net/pixelhumain/pixelhumain.git pixelhumain
fi

if [ -d "${PWD}/modules/" ]
then
echo "modules déja présent"
else
  mkdir modules
fi

cd modules

for repo in {"citizenToolKit","co2","api","dda","news","interop","graph","eco","chat","survey","map","places","cotools","costum"}; do 
  if [ -d "${PWD}/${repo}/" ]
  then
  echo "${repo} déja présent"
  cd "$repo"
  git reset --hard HEAD
  git fetch origin master
  git pull origin master
  cd ..
  else
  git clone "https://gitlab.adullact.net/pixelhumain/${repo}.git" "$repo"
  fi
done


cd ..
cd pixelhumain
# git reset
git reset --hard HEAD
git fetch origin master
git pull origin master

cd ..
cd ..

# Setup configuration for MongoDB
if [ -f "${PWD}/code/pixelhumain/ph/protected/config/dbconfig.php" ]
then
echo "configuration mongodb déja présente : $PWD/code/pixelhumain/ph/protected/config/dbconfig.php"
else
  cat > "${PWD}/code/pixelhumain/ph/protected/config/dbconfig.php" <<EOF
  <?php

if(getenv("MONGO_dsn")){
    \$dsn = getenv("MONGO_dsn");
}
if(!getenv("MONGO_dsn")){
    \$username = getenv("MONGO_username");
    \$password = getenv("MONGO_password");
    \$dbname = getenv("MONGO_dbname");
    \$port = getenv("MONGO_port");
    \$host = getenv("MONGO_host");
    \$authSource = getenv("MONGO_authSource");
    

    // faire dns mongodb en fonction si les champ sont présent ou pas
    \$dsn = "mongodb://";
    if(\$username && \$password){
    \$dsn .= \$username.":".\$password;
    }
    if(\$host){
        \$dsn .= "@".\$host;
    }
    if(\$port){
        \$dsn .= ":".\$port;
    }
    if(\$dbname){
        \$dsn .= "/".\$dbname;
    }
    if(\$authSource){
        \$dsn .= "?authSource=".\$authSource;
    }
}

  \$dbconfig = array(
      'class' => 'mongoYii.EMongoClient',
      'server' => \$dsn,
      'db' => \$dbname,
  );
  \$dbconfigtest = array(
      'class' => 'mongoYii.EMongoClient',
      'server' => \$dsn,
      'db' => 'pixelhumaintest',
  );
EOF
fi

if [ -d "${PWD}/code/pixelhumain/ph/assets/" ]
then
echo "assets déja présent"
rm -rf "${PWD}/code/pixelhumain/ph/assets/*"
chmod -R 777 "${PWD}/code/pixelhumain/ph/assets/"
else
  mkdir "${PWD}/code/pixelhumain/ph/assets/"
  chmod -R 777 "${PWD}/code/pixelhumain/ph/assets/"
fi

if [ -d "${PWD}/code/pixelhumain/ph/protected/runtime/" ]
then
echo "runtime déja présent"
rm -rf "${PWD}/code/pixelhumain/ph/protected/runtime/*"
chmod -R 777 "${PWD}/code/pixelhumain/ph/protected/runtime/"
else
  mkdir "${PWD}/code/pixelhumain/ph/protected/runtime/"
  chmod -R 777 "${PWD}/code/pixelhumain/ph/protected/runtime/"
fi

if [ -d "${PWD}/code/pixelhumain/ph/upload/" ]
then
echo "upload déja présent"
rm -rf "${PWD}/code/pixelhumain/ph/upload/*"
chmod -R 777 "${PWD}/code/pixelhumain/ph/upload/"
else
  mkdir "${PWD}/code/pixelhumain/ph/upload/"
  chmod -R 777 "${PWD}/code/pixelhumain/ph/upload/"
fi

if [ -d "${PWD}/code/pixelhumain/ph/vendor/" ]
then
echo "vendor déja présent"
rm -rf "${PWD}/code/pixelhumain/ph/vendor/"
rm -rf "${PWD}/code/pixelhumain/ph/composer.lock"
fi

if [ -d "${PWD}/code/pixelhumain/ph/web/" ]
then
echo "web déja présent"
rm -rf "${PWD}/code/pixelhumain/ph/web/*"
chmod -R 777 "${PWD}/code/pixelhumain/ph/web/"
else
  mkdir "${PWD}/code/pixelhumain/ph/web/"
  chmod -R 777 "${PWD}/code/pixelhumain/ph/web/"
fi