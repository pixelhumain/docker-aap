# Docker image co

## fichiers et repertoires

### Dockerfile
fichier dockerfile pour créer l'image de co

### copyrepo.sh
copie ou met à jour les repo dans le repertoire code/

### repertoire code/
le repertoire avec le code qui est créer ou mise à jour par copyrepo.sh

### repertoire conf/
repertoire de configuration de l'image pour apache et php

### repertoire conf-yii/
repertoire de configuration pour yii

## commande pour mettre à jour repo et build image

```sh
./copyrepo.sh
VERSION=1.0.35 ./build.sh
```

