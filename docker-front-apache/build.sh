#!/bin/sh
set -e

if [ -z "${VERSION}" ]; then
# Définir VERSION comme un horodatage (format : YYYYMMDDHHMMSS)
VERSION=$(date +"%Y%m%d%H%M%S")
fi

TAG="latest"

docker build --pull --rm -t communecter/communecter-apache:$VERSION .
docker tag communecter/communecter-apache:$VERSION communecter/communecter-apache:$VERSION
docker push communecter/communecter-apache:$VERSION
docker tag communecter/communecter-apache:$VERSION communecter/communecter-apache:$TAG
docker push communecter/communecter-apache:$TAG



