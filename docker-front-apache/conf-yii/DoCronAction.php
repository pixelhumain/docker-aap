<?php

namespace PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cron;

use CAction, Yii, Cron;
class DoCronAction extends \PixelHumain\PixelHumain\components\Action {
	
	//Process the cron inside the cron collection
	//TODO SBAR - Add security with a token or an id of super admin ?
	public function run() {
		if($_GET["crontoken"]==Yii::app()->params["crontoken"]){
			Cron::processCron();
		}
	}
}