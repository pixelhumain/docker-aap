<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks\TaskProcessor;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks\TaskQueue;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubCron;
use Rest;
use Yii;
class ProcessCron extends \PixelHumain\PixelHumain\components\Action
{

        public function run()
        {
        if($_GET["crontoken"]==Yii::app()->params["crontoken"]){
                $taskQueue = new TaskQueue();
                $taskProcessor = new TaskProcessor($taskQueue, null);
                if($taskProcessor->isHasCompleteTasks()){
                    $taskProcessor->processAllTasks();
                }else{
                    ActivitypubCron::processCron();
                }
                return Rest::json(["status" => 1]);
        }

        }

}
