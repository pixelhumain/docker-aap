<?php 

if(getenv("PARAM_mail_host")){
$transportOptions = array(
    'host'=>getenv("PARAM_mail_host")
);

if(getenv("PARAM_mail_username")){
$transportOptions['username'] = getenv("PARAM_mail_username");
}

if(getenv("PARAM_mail_password")){
$transportOptions['password'] = getenv("PARAM_mail_password");
}

if(getenv("PARAM_mail_port")){
$transportOptions['port'] = getenv("PARAM_mail_port");
}

if(getenv("PARAM_mail_encryption")){
$transportOptions['encryption'] = getenv("PARAM_mail_encryption");
}

}

if($transportOptions ?? false){
$mailConfig = array(
        'class' => 'ext.mail.YiiMail',
        'transportType' => 'smtp',
    'transportOptions'=>$transportOptions,
        'viewPath' => 'application.views.emails',
        'logging' => true,
        'dryRun' => false
);
} else {
$mailConfig = array(
        'class' => 'ext.mail.YiiMail',
        'transportType' => 'php',
        'viewPath' => 'application.views.emails',
        'logging' => true,
        'dryRun' => false
);
}

$mailConfigTest = array(
	'class' => 'ext.mail.YiiMail',
	'transportType' => 'smtp',
    'transportOptions'=>array(
      'host'=>'smtp.gmail.com',
      'username'=>'contact@pixelhumain.com',
      'password'=>'2210pixel_$$',
      'port'=>'465',
      'encryption'=>'ssl',
    ),
	'viewPath' => 'application.views.emails',
	'logging' => true,
	'dryRun' => false
);

if(getenv("PARAM_cows_enable")){
$cows = array(
    "enable" => getenv("PARAM_cows_enable"),
);
if(getenv("PARAM_cows_serverUrl")){
$cows['serverUrl'] = getenv("PARAM_cows_serverUrl");
}
if(getenv("PARAM_cows_pingNorificationUrl")){
$cows['pingNorificationUrl'] = getenv("PARAM_cows_pingNorificationUrl");
}
if(getenv("PARAM_cows_pingNewPostUrl")){
$cows['pingNewPostUrl'] = getenv("PARAM_cows_pingNewPostUrl");
}
if(getenv("PARAM_cows_pingRefreshCoformAnswerUrl")){
$cows['pingRefreshCoformAnswerUrl'] = getenv("PARAM_cows_pingRefreshCoformAnswerUrl");
}
if(getenv("PARAM_cows_pingNewEventUrl")){
$cows['pingNewEventUrl'] = getenv("PARAM_cows_pingNewEventUrl");
}
if(getenv("PARAM_cows_pingRefreshViewUrl")){
$cows['pingRefreshViewUrl'] = getenv("PARAM_cows_pingRefreshViewUrl");
}
if(getenv("PARAM_cows_pingActionManagement")){
$cows['pingActionManagement'] = getenv("PARAM_cows_pingActionManagement");
}
if(getenv("PARAM_cows_pingUpdateCostum")){
$cows['pingUpdateCostum'] = getenv("PARAM_cows_pingUpdateCostum");
}
if(getenv("PARAM_cows_pingStripeCheckout")){
    $cows['pingStripeCheckout'] = getenv("PARAM_cows_pingStripeCheckout");
}
if(getenv("PARAM_cows_pingCommentUrl")){
    $cows['pingCommentUrl'] = getenv("PARAM_cows_pingCommentUrl");
}

if(getenv("PARAM_cows_serverUrl_client")){
    $cowsClient = array(
        "enable" => getenv("PARAM_cows_enable"),
    );
    $cowsClient['serverUrl'] = getenv("PARAM_cows_serverUrl_client");
if(getenv("PARAM_cows_pingNorificationUrl_client")){
    $cowsClient['pingNorificationUrl'] = getenv("PARAM_cows_pingNorificationUrl_client");
}
if(getenv("PARAM_cows_pingNewPostUrl_client")){
    $cowsClient['pingNewPostUrl'] = getenv("PARAM_cows_pingNewPostUrl_client");
}
if(getenv("PARAM_cows_pingRefreshCoformAnswerUrl_client")){
    $cowsClient['pingRefreshCoformAnswerUrl'] = getenv("PARAM_cows_pingRefreshCoformAnswerUrl_client");
}
if(getenv("PARAM_cows_pingNewEventUrl_client")){
    $cowsClient['pingNewEventUrl'] = getenv("PARAM_cows_pingNewEventUrl_client");
}
if(getenv("PARAM_cows_pingRefreshViewUrl_client")){
    $cowsClient['pingRefreshViewUrl'] = getenv("PARAM_cows_pingRefreshViewUrl_client");
}
if(getenv("PARAM_cows_pingActionManagement_client")){
    $cowsClient['pingActionManagement'] = getenv("PARAM_cows_pingActionManagement_client");
}
if(getenv("PARAM_cows_pingUpdateCostum_client")){
    $cowsClient['pingUpdateCostum'] = getenv("PARAM_cows_pingUpdateCostum_client");
}
if(getenv("PARAM_cows_pingStripeCheckout_client")){
    $cowsClient['pingStripeCheckout'] = getenv("PARAM_cows_pingStripeCheckout_client");
}
if(getenv("PARAM_cows_pingCommentUrl_client")){
    $cowsClient['pingCommentUrl'] = getenv("PARAM_cows_pingCommentUrl_client");
}

}
} else {
    $cows = array(
    "enable" => false,
);
}


if(getenv("PARAM_jwt_secretKey") && getenv("PARAM_jwt_alg") && getenv("PARAM_jwt_refreshTokenKey") && getenv("PARAM_jwt_schema") && getenv("PARAM_jwt_domain")){
$jwt = array(
    "secretKey" => getenv("PARAM_jwt_secretKey"),
    "alg" => getenv("PARAM_jwt_alg"),
    "refreshTokenKey" => getenv("PARAM_jwt_refreshTokenKey"),
    "schema" => getenv("PARAM_jwt_schema"),
    "domain" => getenv("PARAM_jwt_domain"),
);
}

if($jwt && getenv("PARAM_jwt_exchangeTokenKey")){
    $jwt['exchangeTokenKey'] = getenv("PARAM_jwt_exchangeTokenKey");
}

if($jwt && getenv("PARAM_jwt_whiteListServer")){
    $whiteListServerString = getenv("PARAM_jwt_whiteListServer");
    $jwt['whiteListServer'] = array_map('trim', explode(',', $whiteListServerString));
}

if(getenv("PARAM_activitypub_schema") && getenv("PARAM_activitypub_host")){
$activitypub = array(
    "schema" => getenv("PARAM_activitypub_schema"),
    "host" => getenv("PARAM_activitypub_host"),
);
} 

$params = array(
    'name' => 'Communecter',
    'theme' => 'CO2',
    'betaTest' => false,
    'defaultController' => 'CO2',
    'openatlasId' => '555eba56c655675cdd65bf19',
    'communecterId' => '55dafac4e41d75571d848360',
    'uploadUrl' => "upload/",
    'uploadDir' => '',
    'uploadComDir' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'."\\..\\templates\\upload\\dir\\communecter\\collection\\person",
    'isParentOrganizationAdmin' => false,
    'numberOfInvitByPerson' => 10,
    'CO2DomainName' => 'CO2',    
    "logoUrl" => "/images/logo-communecter.png",
    "logoUrl2" => "/images/logoLTxt.jpg",
    "version"=> "02.1.07",
    "versionAssets" => "166000000",
  );

if($cows){
$params['cows'] = $cows;
}

if($cowsClient){
$params['cowsClient'] = $cowsClient;
}

if($jwt){
$params['jwt'] = $jwt;
}

if($activitypub){
$params['activitypub'] = $activitypub;
}

if(getenv("PARAM_name")){
$params['name'] = getenv("PARAM_name");
} else {
$params['name'] = "Communecter";
}

if(getenv("PARAM_adminEmail")){
$params['adminEmail'] = getenv("PARAM_adminEmail");
}

if(getenv("PARAM_contactEmail")){
$params['contactEmail'] = getenv("PARAM_contactEmail");
}

if(getenv("PARAM_captcha")){
$params['captcha'] = getenv("PARAM_captcha");
}

if(getenv("PARAM_captcha_key")){
$params['captcha-key'] = getenv("PARAM_captcha_key");
}

if(getenv("PARAM_serverIp")){
$params['serverIp'] = getenv("PARAM_serverIp");
}

if(getenv("PARAM_crontoken")){
$params['crontoken'] = getenv("PARAM_crontoken");
}

if(getenv("PARAM_mapboxActive")){
$params['mapboxActive'] = getenv("PARAM_mapboxActive");
if(getenv("PARAM_mapboxToken")){
$params['mapboxToken'] = getenv("PARAM_mapboxToken");
}
} else {
$params['mapboxActive'] = false;
}

if(getenv("PARAM_forceMapboxActive")){
$params['forceMapboxActive'] = getenv("PARAM_forceMapboxActive");
} else {
$params['forceMapboxActive'] = false;
}

if(getenv("PARAM_gulp")){
$params['gulp'] = getenv("PARAM_gulp");
} else {
$params['gulp'] = false;
}

if(getenv("PARAM_rocketchatEnabled")){
$params['rocketchatEnabled'] = getenv("PARAM_rocketchatEnabled");

if(getenv("PARAM_rocketchatMultiEnabled")){
$params['rocketchatMultiEnabled'] = getenv("PARAM_rocketchatMultiEnabled");
}

if(getenv("PARAM_rocketchatURL")){
$params['rocketchatURL'] = getenv("PARAM_rocketchatURL");
}

if(getenv("PARAM_rocketAdmin")){
$params['rocketAdmin'] = getenv("PARAM_rocketAdmin");
}

if(getenv("PARAM_rocketAdminPwd")){
$params['rocketAdminPwd'] = getenv("PARAM_rocketAdminPwd");
}

if(getenv("PARAM_adminLoginToken")){
$params['adminLoginToken'] = getenv("PARAM_adminLoginToken");
}

if(getenv("PARAM_adminRocketUserId")){
$params['adminRocketUserId'] = getenv("PARAM_adminRocketUserId");
}
}

if(getenv("PARAM_openbadge_url")){
$params['openbadge'] = array('url' => getenv("PARAM_openbadge_url"));
}

$modulesDir = '/../../../../modules/';
