#!/bin/bash

REPO_URL="https://gitlab.adullact.net/pixelhumain/oceco.git"
REPO_DIR="app"
BRANCH="master"
APP_BUNDLE_FOLDER="../app-bundle"

if [ -d "$REPO_DIR" ]; then
    echo "Le répertoire $REPO_DIR existe déjà. Mise à jour du dépôt..."
    cd "$REPO_DIR"
    git pull origin "$BRANCH"
    cd ..
else
    echo "Clonage du dépôt..."
    git clone -b "$BRANCH" "$REPO_URL" "$REPO_DIR"
fi

cd $REPO_DIR

# verifier que le dossier node_modules existe
if [ -d "node_modules" ]; then
    echo "Le répertoire node_modules existe déjà. Suppression..."
    rm -rf node_modules
fi

# verifier si meteor est installé
if ! [ -x "$(command -v meteor)" ]; then
    echo 'Error: meteor is not installed.' >&2
    exit 1
fi

meteor npm install

# verifier que le dossier $APP_BUNDLE_FOLDER existe
if [ -d "$APP_BUNDLE_FOLDER" ]; then
    echo "Le répertoire $APP_BUNDLE_FOLDER existe déjà. Suppression..."
    rm -rf $APP_BUNDLE_FOLDER
fi

meteor build --directory $APP_BUNDLE_FOLDER --server-only --architecture os.linux.x86_64

# verifier que le fichier bundle.tar.gz existe
if [ -f "bundle.tar.gz" ]; then
    echo "Le fichier bundle.tar.gz existe déjà. Suppression..."
    rm -f bundle.tar.gz
fi

# tar gzip le bundle bundle.tar.gz
# tar -czf bundle.tar.gz -C app-bundle bundle
tar -czf ../bundle.tar.gz -C $APP_BUNDLE_FOLDER bundle

