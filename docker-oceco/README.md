# Docker image oceco


## fichiers et repertoires

### Dockerfile
fichier dockerfile pour créer l'image de oceco

### copyrepo.sh
copie ou met à jour les repo dans le repertoire app/

### repertoire app/
le repertoire avec le code qui est créer ou mise à jour par copyrepo.sh

## commande pour mettre à jour repo et build image

```sh
./copyrepo.sh
VERSION=1.0.0 ./build.sh
```

## variables d'environnement
ROOT_URL=http://localhost:5084
MONGO_URL=mongodb://admin:password@mongo42:27017/communecter?authSource=admin
# MONGO_OPLOG_URL=
PORT=3000
UNIVERSE_I18N_LOCALES=all
METEOR_SETTINGS=
# METEOR_SETTINGS=$(jq -c . settings.json)

## convertion du fichier settings.json en variable d'environnement
  
```sh
jq -c . settings.json
```