#!/bin/sh
set -e

if [ -z "${VERSION}" ]; then
# Définir VERSION comme un horodatage (format : YYYYMMDDHHMMSS)
VERSION=$(date +"%Y%m%d%H%M%S")
fi

TAG="latest"
NODE_VERSION="14.21.4"

docker build --build-arg NODE_VERSION=$NODE_VERSION --pull --rm -t communecter/communecter-oceco:$VERSION .
docker tag communecter/communecter-oceco:$VERSION communecter/communecter-oceco:$VERSION
docker push communecter/communecter-oceco:$VERSION
docker tag communecter/communecter-oceco:$VERSION communecter/communecter-oceco:$TAG
docker push communecter/communecter-oceco:$TAG



