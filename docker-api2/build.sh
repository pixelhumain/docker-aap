#!/bin/sh
set -e

if [ -z "${VERSION}" ]; then
# Définir VERSION comme un horodatage (format : YYYYMMDDHHMMSS)
VERSION=$(date +"%Y%m%d%H%M%S")
fi

TAG="latest"

docker build --pull --rm -t communecter/communecter-api2:$VERSION .
docker tag communecter/communecter-api2:$VERSION communecter/communecter-api2:$VERSION
docker push communecter/communecter-api2:$VERSION
docker tag communecter/communecter-api2:$VERSION communecter/communecter-api2:$TAG
docker push communecter/communecter-api2:$TAG



