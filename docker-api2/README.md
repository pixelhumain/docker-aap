# Docker image api2


## fichiers et repertoires

### Dockerfile
fichier dockerfile pour créer l'image de co

### copyrepo.sh
copie ou met à jour les repo dans le repertoire api2/

### repertoire api2/
le repertoire avec le code qui est créer ou mise à jour par copyrepo.sh

## commande pour mettre à jour repo et build image

```sh
./copyrepo.sh
VERSION=2.0.0 ./build.sh
```

## variables d'environnement

```sh
NODE_ENV=production
JWT_SECRET=
PORT_SERVER=3000
HOST_TO_FETCH=https://www.communecter.org
MONGO_URL=
```

```bash
docker run -p 3000:3000 \
  -e NODE_ENV=production \
  -e JWT_SECRET=your_jwt_secret \
  -e PORT_SERVER=3000 \
  -e HOST_TO_FETCH=https://www.communecter.org \
  -e MONGO_URL=your_mongo_url \
  communecter/communecter-api2:2.0.0
```