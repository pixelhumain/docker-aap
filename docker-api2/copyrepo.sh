#!/bin/bash

REPO_URL="https://gitlab.adullact.net/pixelhumain/api2.git"
REPO_DIR="api2"
BRANCH="eslint"

if [ -d "$REPO_DIR" ]; then
    echo "Le répertoire $REPO_DIR existe déjà. Mise à jour du dépôt..."
    cd "$REPO_DIR"
    git pull origin "$BRANCH"
    cd ..
else
    echo "Clonage du dépôt..."
    git clone -b "$BRANCH" "$REPO_URL" "$REPO_DIR"
fi
