#!/bin/sh
trap "echo 'Stopping container...'; exit 0" SIGTERM
exec node ./build/server.js
